<?php

namespace Pluio\Monitors;

use Predis\Client;
use Pluio\Logger;
use Pluio\Communication\Slack;
use Predis\Connection\ConnectionException;

/**
 * Attemps to connect to a Redis server and Ping for a response
 *
 */
class RedisMonitor
{
    /**
     * Hostname/IP of the Redis server
     */
    private $_host;

    /**
     * Password (Primary as of Azure terminology)
     */
    private $_password;

    /**
     * Port
     */
    private $_port;

    /**
     * SSL or non SSL
     */
    private $_scheme;

    /**
     * Handle to a \Redis object instance
     */
    private $_redis;

    /**
     * Pluio\Logger instance handler
     */
    private $_logger;

    /**
     * Slack object instance
     */
    private $_slack;

    /**
     * @param $host Hostname/IP of the Redis server
     * @param $password  Password (Primary as of Azure terminology)
     * @param $port Port
     * @param $ssl SSL or non SSL (used in Predis)
     * @param $abort Abort on timeout (used in Predis)
     * @return $this
     */
    public function __construct(string $host = '', string $password = '', string $port = '', string $ssl = '', string $abort = '')
    {
        if ($host) {
            $this->_host = $host;
            $this->_password = $password;
            $this->_port = $port;
            $this->_ssl = $ssl;
            $this->_abort_connect = $abort;
        } else {
            $this->_host = \getenv('REDIS_HOST');
            $this->_password = \getenv('REDIS_PASSWORD');
            $this->_port = \getenv('REDIS_PORT_NONSSL');
            $this->_ssl = \getenv('REDIS_SSL');
            $this->_abort_connect = \getenv('REDIS_ABORT_CONNECT');
        }

        $this->_slack = new Slack();
        $this->_logger = new Logger;
        return $this;
    }

    /**
     * Connects to Redis
     */
    private function _redisServer()
    {
        $conn = [
            'scheme' => 'tcp',
            'host' => $this->_host,
            'password' => $this->_password,
            'port' => $this->_port,
        ];

        try {
            $this->_redis = new Client($conn);
        } catch (ConnectionException $e) {
            // \var_dump($e);
            $this->_slack->post($e->getMessage(), 'danger');
            $this->_logger->error($e->getMessage());
            return false;
        }
        return $this->_redis;
    }

    /**
     * Pings the Redis Server
     *
     * @return bool
     */
    public function ping()
    {
        $checkup = false;
        try {
            $this->_redisServer();
            $pong = $this->_redis->ping();
            if ($pong) {
                // $this->_logger->info($this->_host . ': ' . $pong);
                $checkup = true;
            }
        } catch (ConnectionException $e) {
            \var_dump($e->getMessage());
            $this->_slack->post('Redis Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('Redis Monitor Error: ' . $e->getMessage());
        }
        return $checkup;
    }
}
