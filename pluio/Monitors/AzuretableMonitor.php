<?php

namespace Pluio\Monitors;

use Pluio\Data\NoSQL\Table\Db;
use MicrosoftAzure\Storage\Table\Models\BatchOperations;
use MicrosoftAzure\Storage\Table\Models\Entity;
use MicrosoftAzure\Storage\Table\Models\QueryEntitiesOptions;
use MicrosoftAzure\Storage\Table\Models\Filters\Filter;
//Exceptions
use Pluio\Exceptions\LogicExceptions\NoSQLLogicException;
use Pluio\Exceptions\RuntimeExceptions\NoSQLRuntimeException;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Common\Exceptions\InvalidArgumentTypeException;

/**
 * Model for the Instance Types:
 *
 * - Hotel Franchise
 * - Travel Agency
 * - etc...
 */
class AzureTableMonitor extends Db
{
    /**
     * Schema for the table
     *
     * @var array
     */
    private $_table_schema = ['pong'];

    /**
     * Holds all the entitie's set values
     *
     * @var array
     */
    private $_properties = [];

    /**
     * Holds the returned data from Table
     *
     * @var array
     */
    protected $_query_resultset = null;

    /**
     * Name for the Table
     *
     * @var string
     */
    private $_table_name = 'HealthCheck';

    /**
     * Partition Key
     *
     * @var string
     */
    private $_partition_key = 'ping';

    /**
     * Queries the Table and confirm response to expected string
     */
    public function ping()
    {
        $checkout = false;
        $resulset = $this->setQuery();
        $ping = $resulset->getObject();
        if ($ping->pong == '+PONG') {
            $checkout = true;
        }
        return $checkout;
    }

    /**
     * Make a GET request to retrieve data. GET requests will
     * never cause an update or change to your data because
     * they’re safe and idempotent.
     *
     * Supported Comparison Operators
     * Within a $filter clause, you can use comparison operators to specify the criteria against which to filter the query results.
     * For all property types, the following comparison operators are supported:
     * | Operator              | URI expression |
     * __________________________________________
     * | Equal                 |  eq            |
     * | GreaterThan           |  gt            |
     * | GreaterThanOrEqual    |  ge            |
     * | LessThan              |  lt            |
     * | LessThanOrEqual       |  le            |
     * | NotEqual              |  ne            |
     *
     * Additionally, the following operators are supported for Boolean properties:
     * Additionally, the following operators are supported for Boolean properties:
     * Additionally, the following operators are supported for Boolean properties:
     * Additionally, the following operators are supported for Boolean properties:
     *
     * | Operator       |   URI expression   |
     * _______________________________________
     * | And            |   and              |
     * | Not            |   not              |
     * | Or             |   or               |
     *
     * @see https://docs.microsoft.com/en-us/rest/api/storageservices/Querying-Tables-and-Entities?redirectedfrom=MSDN
     * @param  string $filter Returns only tables or entities that satisfy the specified filter. Note that no more than 15 discrete comparisons are permitted within a $filter string.
     * @param  [type] $top    Returns only the top n tables or entities from the set.
     * @param  [type] $select Returns the desired properties of an entity from the set.
     * @return bool
     */
    public function setQuery(string $filter = null, $top = null, $select = null)
    {
        //Retrieve all entries if no filter is provided
        if (!$filter) {
            $filter = Filter::applyQueryString("PartitionKey eq '$this->_partition_key'");
        } else {
            $filter = Filter::applyQueryString($filter);
        }

        if ($top || $select) {
            $options = new QueryEntitiesOptions();
        } else {
            $options = $filter;
        }

        if ($top) {
            $options->setTop($top);
        }

        if ($select) {
            //RowKey is indispensable for assembling the array's resul set
            $select = array_merge($select, ['RowKey']);
            foreach ($select as $field) {
                $options->addSelectField($field);
            }
            $options->setFilter($filter);
        }
        try {
            try {
                try {
                    $result = $this->tableRestProxy()->queryEntities($this->_table_name, $options);
                    $entities = $result->getEntities();
                    $this->_query_resultset = [];
                    foreach ($entities as $entity) {
                        if ($entity->getTimestamp()) {
                            $this->_query_resultset[$entity->getRowKey()]['timestamp'] = $entity->getTimestamp()->format('Y-m-d H:i:s Z');
                            ;
                        }
                        foreach ($this->_table_schema as $var) {
                            if ($entity->getProperty($var)) {
                                $this->_query_resultset[$entity->getRowKey()][$var] = $entity->getProperty($var)->getValue();
                            } else {
                                if (!$select) {
                                    $this->_query_resultset[$entity->getRowKey()][$var] = null;
                                }
                            }
                        }
                    }
                } catch (ServiceException $ss) {
                    // Handle exception based on error codes and messages.
                    // Error codes and messages are here:
                    // http://msdn.microsoft.com/library/azure/dd179438.aspx
                    // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                    throw new NoSQLRuntimeException($ss->getMessage(), $ss->getCode());
                } catch (\InvalidArgumentException $ae) {
                    throw new NoSQLLogicException($ae->getMessage(), $ae->getCode());
                } catch (\UnexpectedValueException $ve) {
                    throw new NoSQLLogicException($ve->getMessage(), $ve->getCode());
                } catch (InvalidArgumentTypeException $te) {
                    throw new NoSQLLogicException($te->getMessage(), $te->getCode());
                } catch (\Exception $e) {
                    throw new NoSQLLogicException($e->getMessage(), $e->getCode());
                }
            } catch (NoSQLRuntimeException $er) {
                throw new \Exception($er->getMessage(), $er->getCode());
            } catch (NoSQLLogicException $le) {
                throw new \Exception($le->getMessage(), $le->getCode());
            }
        } catch (\Exception $ie) {
            $this->_slack->post('Azure Table Monitor Error: ' . $ie->getMessage(), 'danger');
            $this->_logger->error('Azure Table Monitor Error: ' . $ie->getMessage());
        }

        // $context = ['method' => __METHOD__, 'code' => 8032];
        // $this->_log->info('NoSQL Query operation successful at ' . __METHOD__, $context);

        //An empty resultset; use the accessors instead
        return $this;
    }

    /**
     * Use a POST request to create new resources if does not exits; errors out otherwise.
     *
     * @param  boolean     $batch Flag to signal batch mode operation
     * @return string|bool
     */
    public function post($batch = false)
    {
        try {
            try {
                try {
                    if ($batch) {
                        $count = count($this->_properties);
                        if ($count <= 0) {
                            throw new NoSQLLogicException('Properties argument(s) empty', 1029);
                        }
                        $batchOp = new BatchOperations();
                        for ($i = 0; $i < $count; ++$i) {
                            $entity = new Entity();
                            foreach ($this->_properties[$i] as $k => $v) {
                                $entity->setPartitionKey($this->_partition_key);
                                if ($k == 'id') {
                                    $rowkey = $this->indexPad($v);
                                    $entity->setRowKey($rowkey);
                                }
                                $entity->addProperty($k, $this->propertyType($v), $v);
                            }
                            $batchOp->addInsertEntity($this->_table_name, $entity);
                        }

                        $this->tableRestProxy()->batch($batchOp);
                        $context = ['method' => __METHOD__, 'code' => 8032];
                        $this->_log->info('NoSQL Post in batch operation successful at ' . __METHOD__, $context);
                        return true;
                    } else {
                        $entity = new Entity();
                        $entity->setPartitionKey($this->_partition_key);
                        if ($this->_properties) {
                            foreach ($this->_properties as $k => $v) {
                                if ($k == 'id') {
                                    $rowkey = $this->indexPad($v);
                                    $entity->setRowKey($rowkey);
                                }
                                $entity->addProperty($k, $this->propertyType($v), $v);
                            }
                        } else {
                            throw new NoSQLLogicException('Properties argument(s) empty', 1029);
                        }
                        $this->tableRestProxy()->insertEntity($this->_table_name, $entity);
                        $context = ['method' => __METHOD__, 'code' => 8031];
                        $this->_log->info('NoSQL Post operation successful at ' . __METHOD__, $context);
                        return $rowkey;
                    }
                } catch (ServiceException $ss) {
                    // Handle exception based on error codes and messages.
                    // Error codes and messages are here:
                    // http://msdn.microsoft.com/library/azure/dd179438.aspx
                    // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                    throw new NoSQLRuntimeException($ss->getMessage(), $ss->getCode());
                } catch (\InvalidArgumentException $ae) {
                    throw new NoSQLLogicException($ae->getMessage(), $ae->getCode());
                } catch (\UnexpectedValueException $ve) {
                    throw new NoSQLLogicException($ve->getMessage(), $ve->getCode());
                } catch (InvalidArgumentTypeException $te) {
                    throw new NoSQLLogicException($te->getMessage(), $te->getCode());
                } catch (\Exception $e) {
                    throw new NoSQLLogicException($e->getMessage(), $e->getCode());
                }
            } catch (NoSQLRuntimeException $er) {
                throw new \Exception($er->getMessage(), $er->getCode());
            } catch (NoSQLLogicException $le) {
                throw new \Exception($le->getMessage(), $le->getCode());
            }
        } catch (\Exception $ie) {
            $this->_slack->post('Azure Table Monitor Error: ' . $ie->getMessage(), 'danger');
            $this->_logger->error('Azure Table Monitor Error: ' . $ie->getMessage());

            $this->_revert = true;
            return false;
        }
    }

    /**
     * NOTICE:
     *
     * This is not a listing for EndUsers hence no human hierarchical sorting is necessary.
     *
     * This method overrides the parent::indexPad
     *
     * @return void
     */
    public function indexPad($var, $pad = 10, $side = 'left', $padder = '0')
    {
        return (string) $var;
    }

    /**
     * Make a PATCH request to update a resource. It does not
     * create new properties, just updates existing ones with
     * provided new values.
     *
     * If you need to add new properties, use PUT instead
     *
     * @param  string $rowkey Id column in the MySQL table
     * @return void
     */
    public function patch(string $rowkey)
    {
        try {
            try {
                try {
                    $result = $this->tableRestProxy()->getEntity($this->_table_name, $this->_partition_key, $this->indexPad($rowkey));

                    $entity = $result->getEntity();

                    if ($this->_properties) {
                        foreach ($this->_properties as $k => $v) {
                            $entity->addProperty($k, $this->propertyType($v), $v);
                        }
                    } else {
                        throw new NoSQLLogicException('Properties argument(s) empty', 1029);
                    }
                    $this->tableRestProxy()->updateEntity($this->_table_name, $entity);
                } catch (ServiceException $ss) {
                    // Handle exception based on error codes and messages.
                    // Error codes and messages are here:
                    // http://msdn.microsoft.com/library/azure/dd179438.aspx
                    // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                    throw new NoSQLRuntimeException($ss->getMessage(), $ss->getCode());
                } catch (\InvalidArgumentException $ae) {
                    throw new NoSQLLogicException($ae->getMessage(), $ae->getCode());
                } catch (\UnexpectedValueException $ve) {
                    throw new NoSQLLogicException($ve->getMessage(), $ve->getCode());
                } catch (InvalidArgumentTypeException $te) {
                    throw new NoSQLLogicException($te->getMessage(), $te->getCode());
                } catch (\Exception $e) {
                    throw new NoSQLLogicException($e->getMessage(), $e->getCode());
                }
            } catch (NoSQLRuntimeException $er) {
                throw new \Exception($er->getMessage(), $er->getCode());
            } catch (NoSQLLogicException $le) {
                throw new \Exception($le->getMessage(), $le->getCode());
            }
        } catch (\Exception $ie) {
            $this->_slack->post('Azure Table Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('Azure Table Monitor Error: ' . $e->getMessage());

            $this->_revert = true;
            return false;
        }
        $context = ['method' => __METHOD__, 'code' => 8031];
        $this->_log->info('NoSQL Patch operation successful at ' . __METHOD__, $context);
        return $rowkey;
    }

    /**
     * Use a PUT request to create or update a resource if it
     * already exists.
     *
     * This method supports insertOrMergeEntity() and
     * insertOrReplaceEntity() methods
     *
     * Each method will insert the entity if it does not exist.
     * If the entity already exists, insertOrMergeEntity updates
     * property values if the properties already exist and adds
     * new properties if they do not exist, while
     * insertOrReplaceEntity completely replaces an existing
     * entity with the new properties removing everything else.
     *
     * @param bool $replace Flag to indicate if the entity should be merged or replaced.
     *
     * @return void
     */
    public function put($replace = false)
    {
        try {
            try {
                try {
                    $entity = new Entity();
                    $entity->setPartitionKey($this->_partition_key);
                    if ($this->_properties) {
                        foreach ($this->_properties as $k => $v) {
                            if ($k == 'id') {
                                $rowkey = $this->indexPad($v);
                                $entity->setRowKey($rowkey);
                            }
                            $entity->addProperty($k, $this->propertyType($v), $v);
                        }
                    } else {
                        throw new NoSQLLogicException('Properties argument(s) empty', 1029);
                    }
                    if (!$replace) {
                        $this->tableRestProxy()->insertOrMergeEntity($this->_table_name, $entity);
                    } else {
                        $this->tableRestProxy()->insertOrReplaceEntity($this->_table_name, $entity);
                    }
                } catch (ServiceException $ss) {
                    // Handle exception based on error codes and messages.
                    // Error codes and messages are here:
                    // http://msdn.microsoft.com/library/azure/dd179438.aspx
                    // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                    throw new NoSQLRuntimeException($ss->getMessage(), $ss->getCode());
                } catch (\InvalidArgumentException $ae) {
                    throw new NoSQLLogicException($ae->getMessage(), $ae->getCode());
                } catch (\UnexpectedValueException $ve) {
                    throw new NoSQLLogicException($ve->getMessage(), $ve->getCode());
                } catch (InvalidArgumentTypeException $te) {
                    throw new NoSQLLogicException($te->getMessage(), $te->getCode());
                } catch (\Exception $e) {
                    throw new NoSQLLogicException($e->getMessage(), $e->getCode());
                }
            } catch (NoSQLRuntimeException $er) {
                throw new \Exception($er->getMessage(), $er->getCode());
            } catch (NoSQLLogicException $le) {
                throw new \Exception($le->getMessage(), $le->getCode());
            }
        } catch (\Exception $ie) {
            $this->_slack->post('Azure Table Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('Azure Table Monitor Error: ' . $e->getMessage());

            $this->_revert = true;
            return false;
        }
        $context = ['method' => __METHOD__, 'code' => 8031];
        $this->_log->info('NoSQL Query Put operation successful at ' . __METHOD__, $context);
        return $rowkey;
    }

    /**
     * Make a DELETE request to remove a resource
     *
     * @param  string $rowkey Id column in the MySQL table
     * @return void
     */
    public function delete(string $rowKey)
    {
        try {
            try {
                try {
                    $this->tableRestProxy()->deleteEntity(
                        $this->_table_name,
                        $this->_partition_key,
                        $this->indexPad($rowKey)
                    );
                } catch (ServiceException $ss) {
                    // Handle exception based on error codes and messages.
                    // Error codes and messages are here:
                    // http://msdn.microsoft.com/library/azure/dd179438.aspx
                    // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                    throw new NoSQLRuntimeException($ss->getMessage(), $ss->getCode());
                } catch (\InvalidArgumentException $ae) {
                    throw new NoSQLLogicException($ae->getMessage(), $ae->getCode());
                } catch (\UnexpectedValueException $ve) {
                    throw new NoSQLLogicException($ve->getMessage(), $ve->getCode());
                } catch (InvalidArgumentTypeException $te) {
                    throw new NoSQLLogicException($te->getMessage(), $te->getCode());
                } catch (\Exception $e) {
                    throw new NoSQLLogicException($e->getMessage(), $e->getCode());
                }
            } catch (NoSQLRuntimeException $er) {
                throw new \Exception($er->getMessage(), $er->getCode());
            } catch (NoSQLLogicException $le) {
                throw new \Exception($le->getMessage(), $le->getCode());
            }
        } catch (\Exception $ie) {
            $this->_slack->post('Azure Table Monitor Error: ' . $ie->getMessage(), 'danger');
            $this->_logger->error('Azure Table Monitor Error: ' . $ie->getMessage());

            $this->_revert = true;
            return false;
        }
        $context = ['method' => __METHOD__, 'code' => 8031];
        $this->_log->info('NoSQL Delete operation successful at ' . __METHOD__, $context);
        return true;
    }

    /**
     * Setter for the properties to hydrate the object in a safe way
     *
     * @param  string $property
     * @param  string $value
     * @param  array  $batch    Payload to process in batch mode
     * @return void
     */
    public function set($property, $value, array $batch = [])
    {
        if ($batch) {
            $i = 0;
            foreach ($batch as $id) {
                foreach ($id as $k => $v) {
                    if (in_array($k, $this->_table_schema)) {
                        $this->_properties[$i][$k] = $v;
                    }
                }
                $i++;
            }
        } else {
            if (in_array($property, $this->_table_schema)) {
                $this->_properties[$property] = $value;
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Bubble initialization params to the parent:
     * Pluio\Data\NoSQL\Table\Db
     *
     * @param string $guid   Global Uniquie IDentifyer. Used if
     *                       provided else a new one is generated on the parent
     * @param string $pid    Process ID to identify the consumer thread
     * @param string $vendor
     */
    public function __construct(string $guid = null, string $pid = null, string $vendor = 'Azure')
    {
        return parent::__construct($guid, $pid, $vendor);
    }
}
