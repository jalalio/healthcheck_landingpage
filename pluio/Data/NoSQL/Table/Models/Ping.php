<?php

namespace Pluio\Data\NoSQL\Table\Model;

use Pluio\Data\NoSQL\Table\Db;

/**
 * Methods to CRUD and query Room table
 */
class Ping extends Db
{
    public function query($param)
    {
        return $this->_db;
    }

    public function delete($param)
    {
        return $this->_db;
    }

    public function get($param)
    {
        return $this->_db;
    }

    public function update($param)
    {
        return $this->_db;
    }

    public function create($param)
    {
        return $this->_db;
    }

    public function __construct(string $vendor = 'Azure')
    {
        return parent::__construct($vendor);
    }
}
