<?php

namespace Pluio\Monitors;

use PDO;
use PDOException;
use Pluio\Logger;
use Pluio\Communication\Slack;

/**
 * Probes a Database for a valid response
 */
class MysqlMonitor
{
    /**
     * Hostname
     */
    private $_host;

    /**
     * DB Username
     */
    private $_user;

    /**
     * DB Password
     */
    private $_password;

    /**
     * DB Database name
     */
    private $_benchmark_db;

    /**
     * Pluio\Logger instance handler
     */
    private $_logger;

    /**
     * Slack object instance
     */
    private $_slack;

    /**
     * DB credentials
     *
     * @param $host Hostname
     * @param $user Username
     * @param $password Password
     * @param $benchmark_db Name of the database to probe
     */
    public function __construct(string $host = '', string $user = '', string $password = '', string $benchmark_db = '')
    {
        if ($host) {
            $this->_host = $host;
            $this->_user = $user;
            $this->_password = $password;
            $this->_benchmark_db = $benchmark_db;
        } else {
            $this->_host = \getenv('MYSQL_HOST');
            $this->_user = \getenv('MYSQL_USERNAME');
            $this->_password = \getenv('MYSQL_PASSWORD');
            $this->_benchmark_db = \getenv('MYSQL_DATABASE');
        }

        $this->_slack = new Slack();
        $this->_logger = new Logger();

        return $this;
    }

    /**
     * Runs a SELECT query against the DB
     *
     * @param $query A valid SQL query to run on the DB
     */
    public function ping(string $query)
    {
        $checkup = false;
        try {
            // MySQL with PDO_MYSQL
            $DBH = new PDO(
                "mysql:host=$this->_host;dbname=$this->_benchmark_db",
                            $this->_user,
                $this->_password
            );
            $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $STH = $DBH->query($query);
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            while ($row = $STH->fetch()) {
                if ($row) {
                    $checkup = true;
                }
            }
        } catch (PDOException $e) {
            $this->_slack->post('MySQL Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('MySQL Monitor Error: ' . $e->getMessage());
        } finally {
            $DBH = null;
        }
        return $checkup;
    }
}
