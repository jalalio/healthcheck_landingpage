<?php

namespace Pluio\Communication;

//@see https://github.com/sendgrid/sendgrid-php
use SendGrid\Content;
use SendGrid\Mail;
use SendGrid\Email;
use SendGrid;

/**
 * Wrapper to SendGrid SDK with pre-configured parameters to fit the HealthCheck use cases
 */
class Postman
{
    /**
     * Dispatches a workflow that makes a phone call
     */
    private $_phonecall_email;

    /**
     * Team-wide alerting, email + Slack
     */
    private $_notify_email;

    /**
     * Email signing the email
     */
    private $_from_email;

    /**
     * The healthchecker
     */
    private $_from_name;

    /**
     * Pluio Ninja
     */
    private $_to_name;

    function __construct()
    {
        $this->_phonecall_email = \getenv('HEALTHCHECKER_EMAIL_PHONECALL');
        $this->_notify_email = \getenv('HEALTHCHECKER_EMAIL_NOTIFY');
        $this->_from_name = \appName();
        $this->_from_email = \getenv('HEALTHCHECKER_EMAIL_FROM');
        $this->_to_name = \getenv('HEALTHCHECKER_EMAIL_TO');
        return $this;
    }

    /**
     * Emails to workflow that triggers a phone call
     */
    public function phoneCall(string $subject, string $content)
    {
        return $this->_send($this->_phonecall_email, $subject, $content);
    }

    /**
     * Emails a regular notification email
     */
    public function notify(string $subject, string $content)
    {
        return $this->_send($this->_notify_email, $subject, $content);
    }

    /**
     * Mailer
     */
    private function _send($recipient, $subject, $message_content)
    {
        $from = new Email($this->_from_name, $this->_from_email);
        $to = new Email($this->_to_name, $recipient);
        $content = new Content("text/plain", $message_content);
        $mail = new Mail($from, $subject, $to, $content);
        
        $apiKey = \getenv('SENDGRID_API');
        $sg = new SendGrid($apiKey);
        
        $response = $sg->client->mail()->send()->post($mail);

        return $response;
    }
}
