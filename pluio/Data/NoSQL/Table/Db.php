<?php

namespace Pluio\Data\NoSQL\Table;

use Pluio\Logger;
use Pluio\Communication\Slack;
use Pluio\Data\NoSQL\Table;
//Microsoft Azure library
use MicrosoftAzure\Storage\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Table\Models\Entity;
use MicrosoftAzure\Storage\Table\Models\QueryEntitiesOptions;
use MicrosoftAzure\Storage\Table\Models\Filters\Filter;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;

class Db
{
    /**
     * Global Unique IDentifier. If none is provided
     * on object instantiation a new one is generated.
     *
     * @var string
     */
    protected $_guid;

    private $_slack;

    /**
     *
     * Logger\Log instance
     *
     * @var object
     */
    protected $_log;

    /**
     * Current timestamp
     *
     * @var string
     */
    protected $_timestamp;

    /**
     * Instance of ServicesBuilder::getInstance()->createTableService
     *
     * @var object
     */
    protected $_db;

    //#################################################
    //#####                                       #####
    //                                                #
    /**              Multi-Row Results             **/
    //                                                #
    //#####                                       #####
    //#################################################

    /**
     * returns an indexed array of indexed arrays from the
     * table records returned by the query
     *
     * @return bool|array
     */
    public function getAssocList()
    {
        if ($this->_query_resultset) {
            return $this->_query_resultset;
        } else {
            return null;
        }
    }

    //#################################################
    //#####                                       #####
    //                                                #
    /**             Single-Row Results             **/
    //                                                #
    //#####                                       #####
    //#################################################

    /**
     * Alias of getObject()
     *
     * @param  boolean $reverse
     * @return void
     */
    public function get($reverse = false)
    {
        return $this->getObject($reverse);
    }

    /**
     * Returns a PHP object from a single record
     * in the table. Whilst you can repeat the call
     * to get further rows, one of the functions
     * that returns multiple rows might be
     * more useful.
     *
     * @param  bool   $reverse Flag to provide the firt element
     *                         from the array. Defaults to last one.
     * @return object
     */
    public function getObject($reverse = false)
    {
        if ($this->_query_resultset) {
            if (!$reverse) {
                $iterator = array_pop($this->_query_resultset);
            } else {
                $iterator = array_slice($this->_query_resultset, 1);
            }
            return (object) $iterator;
        } else {
            return null;
        }
    }

    /**
     * Returns an indexed array from a single record in the
     * table. Whilst you can repeat the call to get further rows,
     * one of the functions that returns multiple rows might
     * be more useful
     *
     * @param  boolean $reverse Flag to provide the firt element
     *                          from the array. Defaults to last one.
     * @return void
     */
    public function getRow($reverse = false)
    {
        if ($this->_query_resultset) {
            if (!$reverse) {
                $iterator = array_pop($this->_query_resultset);
            } else {
                $iterator = array_slice($this->_query_resultset, 1);
            }
            return $iterator;
        } else {
            return null;
        }
    }

    /**
     * Wrapper to str_pad() to be used as rowKeys, because
     * Azure Table sorts Lexicographical order, so 1, 111 and
     * 11 are going to be sorted ambigously to the user
     *
     * @param  string  $var    Variable to padd
     * @param  integer $pad    Lenght of the string
     * @param  string  $side   Side to append the padding
     * @param  string  $padder Character to use as pad
     * @return string
     */
    public function indexPad($var, $pad = 10, $side = 'left', $padder = '0')
    {
        switch ($side) {
            case 'left':
                $s = STR_PAD_LEFT;
                break;
            default:
            case 'right':
                $s = STR_PAD_RIGHT;
                break;
            case 'both':
                $s = STR_PAD_BOTH;
                break;
        }
        return str_pad($var, $pad, $padder, $s);
    }

    /**
     * Gets the total count of a resul set
     *
     * @param  string $tableName
     * @param  string $filter
     * @return string
     */
    public function getTotalCount($tableName, $select = [], $filter)
    {
        $options = new QueryEntitiesOptions();
        $options->setSelectFields($select);
        $options->setFilter(Filter::applyQueryString($filter));

        try {
            try {
                $result = $this->_db->queryEntities($tableName, $options);
                $entities = $result->getEntities();
                return count($entities);
            } catch (ServiceException $e) {
                // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                throw new \Exception($e->getMessage(), $e->getCode());
            }
        } catch (\Exception $er) {
            $er->issue('error');
        }
    }

    /**
     * Returns the Entity Data Model (EDM) of a given value
     *
     * @param  mixed $value
     * @return void
     */
    public function propertyType($value)
    {
        return TableEdmType::propertyType($value);
    }

    /**
     * Proxy to manipulate Azure Tables
     *
     * @return object
     */
    public function tableRestProxy()
    {
        return $this->_db;
    }

    /**
     * Gets a valid a connection to the database
     *
     * @param string $vendor Vendor of the NoSQL infrastructure
     */
    public function __construct()
    {
        $this->_log = new Logger();
        $this->_slack = new Slack();
        $datetime = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->_timestamp = $datetime->format('Y-m-d H:i:s Z');

        try {
            try {
                $connectionString = Azure::AzureCredentialsTablesAccount();
                $this->_db = ServicesBuilder::getInstance()->createTableService($connectionString);
            } catch (ServiceException $e) {
                // Fail: Code: 409 Value: Conflict details (if any): {"odata.error":{"code":"EntityAlreadyExists","message":{"lang":"en-US","value":"The specified entity already exists.\nRequestId:8fca2006-0002-013d-5927-181e9d000000\nTime:2017-08-18T13:38:51.5161824Z"}}}.
                throw new \Exception($e->getMessage(), $e->getCode());
            }
        } catch (\Exception $er) {
            $this->_slack->post('Azure Table Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('Azure Table Monitor Error: ' . $e->getMessage());
        }

        return;
    }
}
