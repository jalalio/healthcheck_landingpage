<?php 

namespace Pluio\Communication;

//@see https://github.com/markenwerk/php-slack-client
use Pluio\Communication\Slack\SlackPostman;

/**
 * Posts a message to a Pluio's Slack channel using Webhookslo
 */
class Slack
{
    /**
     * Pluio's Slack Webhook
     */
    private $_webhook;

    /**
     * Slack endpoint
     */
    private $_base_url;

    /**
     * PluioBot
     */
    private $_username;

    /**
     * Channel name
     */
    private $_channel;

    /**
     * Pluio\Communication\Slack\SlackPostman object instance
     */
    private $_slack_handle;

    public function __construct()
    {
        $this->_base_url = \getenv('SLACK_ALERTS_BASEURL');
        $this->_webhook = \getenv('SLACK_ALERTS_WEBHOOK');
        $this->_username = \appName();
        $this->_channel = \getenv('SLACK_CHANNEL');
        $this->_slack_handle = new SlackPostman($this->_base_url, $this->_webhook, $this->_username, $this->_channel);
        return $this;
    }

    /**
     * Post a simple message to Slack
     *
     * @param $message Text of the message
     * @param $level Alert level: Accepted values: "good", "warning", "danger"
     */
    public function post(string $message, $level = 'good')
    {
        $this->_slack_handle->attachment(
            $message,
            $message,
            ['title' => $message, 'link' => null],
            null,
            ['name' => \appName(), 'link' => null, 'icon' => null],
            null,
            [],
            $level,
        ['footer' => null, 'footer_icon' => null, 'ts' => null]
        );
        $this->_slack_handle->post($message, $this->_channel);
        return;
    }
}
