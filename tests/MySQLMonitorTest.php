<?php

use PHPUnit\Framework\TestCase;
use Pluio\Monitors\MysqlMonitor;

class MySQLMonitorTest extends TestCase
{
    /** @test */
    public function it_connects_to_mysql_server()
    {
        $server = new MysqlMonitor();
        $ping = $server->ping("SELECT ping FROM healthcheck WHERE ping='+PONG'");
        $this->assertNotFalse($ping, 'MySQL server is not available');
    }
}
