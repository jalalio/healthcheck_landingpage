<?php

namespace Pluio\Monitors;

use Aws\Sdk;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
use Pluio\Logger;
use Pluio\Communication\Slack;

/**
 * Probes a DynamoDB Table for a valid response
 */
class DynamodbMonitor
{
    /**
     * Endpoint for the region selected
     */
    private $_endpoint;

    /**
     * Preferable Canada
     */
    private $_region;

    /**
     * API version
     */
    private $_version;

    /**
     * Table used for the benchmark
     */
    private $_benchmark_table;

    /**
     * Pluio\Logger instance handler
     */
    private $_logger;

    /**
     * Slack object instance
     */
    private $_slack;

    /**
     * DB credentials
     *
     * @param $host Hostname
     * @param $user Username
     * @param $password Password
     * @param $benchmark_db Name of the database to probe
     */
    public function __construct(string $endpoint = '', string $region = '', string $version = '', string $benchmark_db = '')
    {
        if ($endpoint) {
            $this->_endpoint = $endpoint;
            $this->_region = $region;
            $this->_version = $version;
            $this->_benchmark_table = $benchmark_db;
        } else {
            $this->_endpoint = \getenv('AWS_DYNAMODB_ENDPOINT');
            $this->_region = \getenv('AWS_DYNAMODB_REGION');
            $this->_version = \getenv('AWS_DYNAMODB_VERSION');
            $this->_benchmark_table = \getenv('AWS_DYNAMODB_HEALTHCHECK_TABLE');
        }

        $this->_slack = new Slack();
        $this->_logger = new Logger();

        return $this;
    }

    /**
     * Runs a query against the DB
     *
     */
    public function ping()
    {
        $checkup = false;

        $sdk = new Sdk([
            'endpoint' => $this->_endpoint,
            'region' => $this->_region,
            'version' => $this->_version
        ]);

        $dynamodb = $sdk->createDynamoDb();
        $marshaler = new Marshaler();

        $eav = $marshaler->marshalJson('
            {
                ":ping": "+PONG" 
            }
        ');

        $params = [
            'TableName' => $this->_benchmark_table,
            'KeyConditionExpression' => '#ping = :ping',
            'ExpressionAttributeNames' => ['#ping' => 'ping'],
            'ExpressionAttributeValues' => $eav
        ];

        try {
            $result = $dynamodb->query($params);
            foreach ($result['Items'] as $ping) {
                if ($marshaler->unmarshalValue($ping['ping']) == '+PONG') {
                    $checkup = true;
                }
            }
        } catch (DynamoDbException $e) {
            $this->_slack->post('DynamoDB Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('DynamoDB Monitor Error: ' . $e->getMessage());
        }
        return $checkup;
    }
}
