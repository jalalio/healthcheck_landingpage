<?php

namespace Pluio\Communication\Slack;

use Pluio\Communication\Slack\WebHook\Slack;
use Pluio\Communication\Slack\WebHook\SlackAttachment;
use Pluio\Communication\Slack\WebHook\SlackMessage;

//@see https://github.com/SimonBackx/Slack-PHP-Webhook

class SlackPostman
{
    /**
     * A vendor's Slack instance
     *
     * @var Object
     */
    private $_slack;

    /**
     * Body of the post.
     *
     * @var string
     */
    private $_post;

    /**
     * Who is posting
     *
     * @var string
     */
    private $_default_username = 'PLUIO:Bot';

    /**
     * Team's channel
     *
     * @var string
     */
    private $_default_channel = '#general';

    /**
     * Icon for the post
     *
     * @var string
     */
    private $_default_icon_url = 'https://static.pluio.com/gui/pluio/branding/pluio-icon.png';

    /**
     * Emoji of the post
     *
     * @see https://unicodey.com/emoji-data/table.htm
     * @var string
     */
    private $_default_emoji = ':warning:';

    /**
     * Team's Webhook
     *
     * @var string
     */
    private $_webhook_url = null;

    /**
     * Slack main endpoint
     *
     * @var string
     */
    private $_base_url = null;

    /**
     * Cards with content as the messages
     *
     * @var array
     */
    private $_attachments = [];

    /**
     * Set up Slack
     *
     * @param string  $webhook     Team's integration webhook
     * @param string  $username    Name of the posting entity. i.e. PLUIO:Bot
     * @param string  $channel     Defaults to #general
     * @param string  $icon        Icon of the post
     * @param string  $emoji       Emoji of the post
     * @param boolean $parse_links Extract a summary from links in the content
     */
    public function __construct(
        string $slack_base_url,
        string $webhook,
        string $username = null,
        string $channel = null,
        string $icon = null,
        string $emoji = null,
        $parse_links = false
    ) {
        //Some validation would be nice
        $this->_base_url = $slack_base_url;
        $this->_webhook_url = $webhook;

        if ($username) {
            $this->_default_username = $username;
        }

        if ($channel) {
            $this->_default_channel = $channel;
        }

        if ($icon) {
            $this->_default_icon_url = $icon;
        }

        if ($emoji) {
            $this->_default_emoji = $emoji;
        }

        return;
    }

    /**
     * Pots a message onto a Slack's channel
     *
     * @param  string $message
     * @param  string $channel
     * @param  string $icon
     * @param  string $emoji
     * @return void
     */
    public function post(
        string $post = null,
        string $channel = null,
        string $icon = null,
        string $emoji = null,
        bool $parse_links = false
    ) {
        $this->_post = $this->_slackify($post);

        $slack = new Slack($this->_base_url, $this->_webhook_url);
        $slack->setDefaultUsername($this->_default_username);
        $slack->setDefaultChannel($this->_default_channel);
        $slack->setDefaultIcon($this->_default_icon_url);
        $slack->setDefaultEmoji($this->_default_emoji);
        $slack->setDefaultUnfurlLinks($parse_links);

        $message = new SlackMessage($slack);
        $message->setText($this->_post);

        if ($channel) {
            $endpoint = $channel;
        } else {
            $endpoint = $this->_default_channel;
        }
        $message->setChannel($endpoint);

        if ($this->_attachments) {
            foreach ($this->_attachments as $att) {
                $message->addAttachment($att);
            }
        }

        try {
            if (!$message->send()) {
                throw new \Exception("Posting to Slack {$endpoint} failed using Webhook {$this->_webhook_url}", 1027);
            }
        } catch (\Exception $e) {
            // \var_dump($e->getMessage());
            return false;
        }

        //Log the successful event here

        return;
    }

    /**
     * Attaches content inside a card with advanced formatting
     *
     * @see https://api.slack.com/docs/message-formatting
     *
     * @param string $fallback A plain-text summary of the attachment. This text will be used in clients that don't show formatted text (eg. IRC, mobile notifications) and should not contain any markup.
     * @param array  $title    ['title'=>'Title', 'link'=>'url']
     * @param string $body     *Optional text* that appears within the attachment
     * @param string $pretext  Optional text that appears above the attachment block
     * @param array  $author   author parameters
     *                         The author parameters will display a small section at the top of a message attachment that can contain the following fields:
     *                         author_name
     *                         Small text used to display the author's name.
     *                         author_link
     *                         A valid URL that will hyperlink the author_name text mentioned
     *                         above. Will only work if author_name is present.
     *                         author_icon
     *                         A valid URL that displays a small 16x16px image to the left of
     *                         the author_name text. Will only work if author_name is present.
     * @param string $image    i.e. http://www.domain.com/picture.jpg
     * @param array  $fields   fields
     *                         Fields are defined as an array, and hashes contained within it will be displayed in a table inside the message attachment.
     *                         title
     *                         Shown as a bold heading above the value text. It cannot contain markup and will be escaped for you.
     *                         value
     *                         The text value of the field. It may contain standard message
     *                         markup and must be escaped as normal. May be multi-line.
     *                         short
     *                         An optional flag indicating whether the value is short enough to be displayed side-by-side with other values.
     * @param string $color    Accepted values: "good", "warning", "danger" or any hex color code
     * @see https://api.slack.com/docs/message-attachments
     * @see https://get.slack.help/hc/en-us/articles/202288908-How-can-I-add-formatting-to-my-messages-
     * @param  string $markdown_enabled Enables message formatting () in attachements. Possible values: "pretext", "text", "fields"
     * @return void
     */
    public function attachment(
        string $summary,
        string $body,
        array $title = [],
        string $pretext = null,
        array $author = [],
        string $image = null,
        array $fields = [],
        string $color = 'good',
        array $footer = [],
        $markdown_enabled = 'text'
    ) {
        $attachment = new SlackAttachment($summary);
        $attachment->setTitle($this->_slackify($title['title']), $title['link']);
        $attachment->setText($this->_slackify($body));
        $attachment->setPretext($this->_slackify($pretext));
        $attachment->setAuthor($author['name'], $author['link'], $author['icon']);
        $attachment->setImage($image);
        $attachment->enableMarkdownFor($markdown_enabled);

        if ($fields) {
            foreach ($fields as $field) {
                $attachment->addField($field['title'], $field['value'], $field['short']);
            }
        }
        $attachment->setColor($color);
        $attachment->setFooter($footer);

        if ($attachment) {
        }

        return $this->_attachments[] = $attachment;
    }

    /**
     * Encodes message into a Slack clean form.
     *
     * @param  string $string
     * @return string
     */
    private function _slackify($string = null)
    {
        if (!$string) {
            return;
        }
        // $string = preg_replace("/\n/m", '\n', $string);
        $slack_target = ['&', '<', '>'];
        $slack_replace = ['&amp;', '&lt;', '&gt;'];
        return \str_replace($slack_target, $slack_replace, $string);
    }
}
