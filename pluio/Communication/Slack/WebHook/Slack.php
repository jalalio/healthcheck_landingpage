<?php

namespace Pluio\Communication\Slack\WebHook;

use GuzzleHttp\Client;

//Taken from
//@link https://raw.githubusercontent.com/SimonBackx/Slack-PHP-Webhook/master/slack.php
/*
    The MIT License (MIT)

    Copyright (c) 2015 Simon Backx

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

/**
 *  Main Object. Construct it by passing your webhook url from slack.com (e.g. https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX)
 *  Needed for posting Slack Messages
 */
class Slack
{
    // WebhookUrl e.g. https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX
    private $base_url;
    private $webhook;

    // Maximum amount of posts per page load. Keep this low for safety. Each post you place requires your server to send some data to slack, and that can take some time.
    const MAX_POSTS = 10;

    private $posts = 0;

    // Empty => Default username set in Slack Webhook integration settings
    private $username;

    // Empty => Default channel set in Slack Webhook integration settings
    private $channel;

    // Empty => Default icon set in Slack Webhook integration settings
    private $icon_url;

    // Empty => Default icon set in Slack Webhook integration settings
    private $icon_emoji;

    // Unfurl links: automatically fetch and create attachments for URLs
    // Empty = default (false)
    private $unfurl_links;

    public function __construct(string $slackbaseurl, string $webhookUrl)
    {
        $this->base_url = $slackbaseurl;
        $this->webhook = $webhookUrl;
    }

    public function __isset($property)
    {
        return isset($this->$property);
    }

    public function send(SlackMessage $message)
    {
        if ($this->posts >= self::MAX_POSTS) {
            return false;
        }
        $this->posts++;

        // Loading defaults
        if (isset($this->username)) {
            $username = $this->username;
        }
        if (isset($this->channel)) {
            $channel = $this->channel;
        }
        if (isset($this->icon_url)) {
            $icon_url = $this->icon_url;
        }
        if (isset($this->icon_emoji)) {
            $icon_emoji = $this->icon_emoji;
        }
        if (isset($this->unfurl_links)) {
            $unfurl_links = $this->unfurl_links;
        }

        // Overwrite/create defaults
        if (isset($message->username)) {
            $username = $message->username;
        }
        if (isset($message->channel)) {
            $channel = $message->channel;
        }
        if (isset($message->icon_url)) {
            $icon_url = $message->icon_url;
        }
        if (isset($message->icon_emoji)) {
            $icon_emoji = $message->icon_emoji;
        }
        if (isset($message->unfurl_links)) {
            $unfurl_links = $message->unfurl_links;
        }

        $data = [
            'text' => $message->text
        ];
        if (isset($username)) {
            $data['username'] = $username;
        }
        if (isset($channel)) {
            $data['channel'] = $channel;
        }
        if (isset($icon_url)) {
            $data['icon_url'] = $icon_url;
        } else {
            if (isset($icon_emoji)) {
                $data['icon_emoji'] = $icon_emoji;
            }
        }

        if (isset($unfurl_links)) {
            $data['unfurl_links'] = $unfurl_links;
        }

        if (isset($message->attachments)) {
            $attachments = [];
            foreach ($message->attachments as $attachment) {
                $attachments[] = $attachment->toArray();
            }
            $data['attachments'] = $attachments;
        }

        try {
            $payload = json_encode($data);
            $client = new Client(
                [
                'base_uri' => $this->base_url]
            );

            $response = $client->post($this->webhook, ['body' => $payload]);

            if ($response->getStatusCode() == 200) {
                return true;
            }
        } catch (\Exception $e) {
            // \var_dump($e->getMessage());
            // die(__METHOD__);
            return false;
        }
    }

    public function setDefaultUnfurlLinks($unfurl)
    {
        $this->unfurl_links = $unfurl;
        return $this;
    }

    public function setDefaultChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    public function setDefaultUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setDefaultIcon($url)
    {
        $this->icon_url = $url;
        return $this;
    }

    public function setDefaultEmoji($emoji)
    {
        $this->icon_emoji = $emoji;
        return $this;
    }
}
