<?php

namespace Pluio;

//@see https://github.com/Seldaek/monolog
use Monolog\Logger as Monolog;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SyslogHandler;

/**
 * Wrapper to Monolog Logger objects in order to
 * customize to Pluio HealthChekeck needs
 */
class Logger
{
    /**
     * Handle to a Monolog\Logger instance
     */
    private $_monolog;

    /**
     * Writes log entries to the syslog()
     */
    public function __construct()
    {
        $this->_monolog = new Monolog('HealthCheck@' . \gethostname());
        $syslog = new SyslogHandler(\gethostname());
        $this->_monolog->pushHandler($syslog);
        return $this;
    }

    /**
     * Logs informational events
     */
    public function info($message, $payload = [])
    {
        return $this->_monolog->info($message, $payload);
    }

    /**
     * Logs error events
     */
    public function error($message, $payload = [])
    {
        return $this->_monolog->error($message, $payload);
    }
}
