<?php

namespace Pluio;

/**
 * Just a helper object for declarative OOP
 *
 * @see http://php.net/manual/en/language.operators.comparison.php
 */
class Compare
{
    /**
     * Just a switch
     */
    private $_mallet = false;

    /**
     * TRUE if $a is equal to $b, and they are of the same type.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isIdentical($value, $reference)
    {
        if ($value === $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }

    /**
     * TRUE if $a is equal to $b after type juggling.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isEqual($value, $reference)
    {
        if ($value == $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }

    /**
     * TRUE if $a is strictly greater than $b.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isGreaterThan($value, $reference)
    {
        if ($value > $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }

    /**
     * TRUE if $a is greater than or equal to $b.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isGreaterThanOrEqual($value, $reference)
    {
        if ($value >= $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }

    /**
     * TRUE if $a is strictly less than $b.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isLessThan($value, $reference)
    {
        if ($value < $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }

    /**
     * TRUE if $a is not equal to $b after type juggling.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isNotEqual($value, $reference)
    {
        if ($value != $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }

    /**
     * An integer less than, equal to, or greater than zero when $a is respectively less than, equal to, or greater than $b.
     *
     * @param $value Value to be evaluated
     * @param $reference Value to evaluate gainst
     * @return bool
     */
    public function isSpaceship($value, $reference)
    {
        if ($value <=> $reference) {
            $this->_mallet = true;
        }
        return $this->_mallet;
    }
}
