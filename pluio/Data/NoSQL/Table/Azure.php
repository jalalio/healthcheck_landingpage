<?php

namespace Pluio\Data\NoSQL\Table;

/**
 * Microsoft Azure is the vendor for storage and database tables.
 *
 * This object provides the credentials to consume their services
 */
class Azure
{
    /**
     * Connection string to the Azure Storage Service
     *
     * @var string
     */
    protected $_connection_string;

    /**
     * Compiles the connection configuration to an Azure Storage container.
     *
     * You should access this object using specialized child objects according to the given task
     *
     * @param array $params
     */
    protected function __construct($account)
    {
        //Development should use http to avoid SSL conflicts with cURL
        //Production should use https
        $this->_connection_string = sprintf(
        'DefaultEndpointsProtocol=%s;AccountName=%s;AccountKey=%s',
        $this->_getConfig($account, 'PROTOCOL'),
        $this->_getConfig($account, 'ACCOUNT'),
        $this->_getConfig($account, 'KEY')
        );
        return $this->_connection_string;
        //DefaultEndpointsProtocol=https;AccountName=staticpluioshahla;AccountKey=+fd+nzC5SAbH41XuTTmhg0uoaIBUVM4rkzHe2qZ44Fsctc11aiHkOx7F3NfjAEyqGv8SUpsONoW8wlw36zlYkQ==;EndpointSuffix=core.windows.net
    }

    /**
     * Facade to the Azure Connection Credentials for the Static Account (Public ACL)
     *
     * @return string Connection string to Azure Storage Services
     */
    public static function AzureCredentialsStaticAccount()
    {
        $azure = new Azure('STATIC');
        //http;staticpluioshahla;+fd+nzC5SAbH41XuTTmhg0uoaIBUVM4rkzHe2qZ44Fsctc11aiHkOx7F3NfjAEyqGv8SUpsONoW8wlw36zlYkQ==;core.windows.net
        return $azure->_connection_string;
    }

    /**
     * Facade to the Azure Connection Credentials for the Operational Blobs Account (Private ACL)
     *
     * @return string Connection string to Azure Storage Services
     */
    public static function AzureCredentialsOpsAccount()
    {
        $azure = new Azure('OPS');
        // DefaultEndpointsProtocol=https;AccountName=datapluioshahla;AccountKey=UkyegAmwk4pP/RYmkygVmi6QdN/OFic3IJ32/L25a0JEoJFI17d/5+Ix7oi3lgACuXOrEIm98a+tNhRMQfdUNg==;EndpointSuffix=core.windows.net
        return $azure->_connection_string;
    }

    /**
     * Facade to the Azure Connection Credentials for the Operational Table Account (Private ACL)
     *
     * @return string Connection string to Azure Storage Services
     */
    public static function AzureCredentialsTablesAccount()
    {
        $azure = new Azure('TABLES');
        // DefaultEndpointsProtocol=https;AccountName=datapluioshahla;AccountKey=UkyegAmwk4pP/RYmkygVmi6QdN/OFic3IJ32/L25a0JEoJFI17d/5+Ix7oi3lgACuXOrEIm98a+tNhRMQfdUNg==;EndpointSuffix=core.windows.net
        return $azure->_connection_string;
    }

    /**
     * Grabs the credentials from the master configuration ledger
     *
     * AZURE_OPS_PROTOCOL
     * AZURE_OPS_ACCOUNT
     * AZURE_OPS_KEY
     * AZURE_OPS_SUFFIX
     *
     * @param  string $account i.e. OPS
     * @param  string $param   i.e . PROTOCOL
     * @return void
     */
    private function _getConfig($account, $param)
    {
        $account_param = 'AZURE_' . strtoupper($account) . '_' . $param;
        return \getenv($account_param);
    }
}
