<?php

//Composer
include 'vendor/autoload.php';
//Load Environment Variables
//@see https://github.com/vlucas/phpdotenv
$config = new Dotenv\Dotenv(__DIR__);
$config->load();

if(getenv('HEALTHCHECKER_ENVIRONMENT') == 'development')
{
    //Turn this off in Production
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

//Lets name this app
function appName()
{
    $hostname = null;
    if (getenv('HEALTHCHECKER_HOSTNAME')) {
        $hostname = getenv('HEALTHCHECKER_HOSTNAME');
    } else {
        $hostname = gethostname();
    }
    $app = getenv('HEALTHCHECKER') . "@" . $hostname;
    return $app;
}