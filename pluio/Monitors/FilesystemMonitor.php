<?php

namespace Pluio\Monitors;

use Pluio\Logger;
use Pluio\Communication\Slack;

/**
 * Monitors a file or directory for write access for the Server
 */
class FilesystemMonitor
{
    /**
     * Path to the file or directory to check up on
     */
    private $_path;

    /**
     * Pluio\Logger instance handler
     */
    private $_logger;

    /**
     * Slack object instance
     */
    private $_slack;

    /**
     * @param $path Path to the file or directory to check up on
     */
    public function __construct(string $path)
    {
        $this->_path = $path;
        $this->_slack = new Slack();
        $this->_logger = new Logger();
        return $this;
    }

    /**
     * Checks out if paht is writeable to the Webserver
     *
     * @return bool
     */
    public function isWritable()
    {
        if (is_writable($this->_path)) {
            return true;
        }
        $alert_message = 'File Monitor Error: ' . $this->_path . ' is not writable or doesnt exist!';
        $this->_slack->post($alert_message, 'danger');
        $this->_logger->error($alert_message);
        return false;
    }
}
