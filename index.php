<?php

include 'bootstrap.php';

use Pluio\Messages;
use Pluio\Logger;
use Pluio\Communication\Postman;
use cbednarski\Pulse\Pulse;
use Pluio\Stopwatch;
use Pluio\Monitors\MysqlMonitor;
use Pluio\Monitors\FilesystemMonitor;
use Pluio\Monitors\RedisMonitor;
use Pluio\Monitors\DynamodbMonitor;
use Pluio\Monitors\AzuretableMonitor;
use Pluio\Monitors\MongodbMonitor;

$timeGlobalExecution = new Stopwatch('GlobalTime');
$timeGlobalExecution->setStart();

$logger = new Logger;

//Testing
// $mailer = new Postman;
// $messages = new Messages();
// $log->info($messages->pong);
// $mailer->notify($bad->alert,$bad->alert);

//@see https://github.com/cbednarski/pulse
$pulse = new Pulse();

//DynamoDB
if (\filter_var(\getenv('AWS_DYNAMODB_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $timeGlobalExecution->setInterval('D6B');
    $pulse->add('Check DynamoDB', function () {
        $dynamodb = new DynamodbMonitor();
        $dynamo = $dynamodb->ping();
        return $dynamo;
    });
}

//Azure Table
if (\filter_var(\getenv('AZURE_TABLES_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $timeGlobalExecution->setInterval('T3E@A3E');
    $pulse->add('Check Azure Table', function () {
        $table = new AzuretableMonitor();
        return $table->ping();
    });
}

//MySQL
if (\filter_var(\getenv('MYSQL_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $timeGlobalExecution->setInterval('M3L');
    $pulse->addCritical('Check MySQL Cluster', function () {
        $sabito = new MysqlMonitor();
        $dblive = $sabito->ping("SELECT ping FROM healthcheck WHERE ping='+PONG'");
        return $dblive;
    });
}

//Redis
if (\filter_var(\getenv('REDIS_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $timeGlobalExecution->setInterval('R3S');
    $pulse->addCritical('Check Redis Cluster', function () {
        $redis = new RedisMonitor();
        return $redis->ping();
    });
}

//MongoDB
if (\filter_var(\getenv('MONGODB_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $timeGlobalExecution->setInterval('M5B');
    $pulse->addCritical('Check MongoDB Cluster', function () {
        $mongo = new MongodbMonitor();
        return $mongo->ping(['ping']);
    });
}

//Filesystem
if (\filter_var(\getenv('HEALTHCHECK_FILESYSTEM_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $timeGlobalExecution->setInterval('F8M');
    $pulse->addCritical('Check File System', function () {
        $filesystem = new FilesystemMonitor(\getenv('HEALTHCHECK_FILESYSTEM_PATH'));
        return $filesystem->isWritable();
    });
}

//Notifications

//System banner
$pulse->addWarning("Welcome to", function () {
return '<br />' . gethostname();
});

//User banner from .env
if (\filter_var(\getenv('BANNER_NOTICE_ENABLED'), FILTER_VALIDATE_BOOLEAN)) {
    $pulse->addWarning("Notes", function () {
        return '<br />' . \getenv('BANNER_NOTICE');
    });
}

$timeGlobalExecution->setEnd();

$pulse->addInfo('Deltas (MSec)', function () use ($timeGlobalExecution, $logger) {
    $deltas = $timeGlobalExecution->getSplicedRutime('msec');
    $logger->info('Deltas', $deltas);
    $string = [];
    $string[] = '<pre>';
    $string[] = implode('<br />', $deltas);
    $string[] = '</pre>';
    $string = implode('<br />', $string);
    return $string;
});

//Push the Healthecks
$pulse->check();
