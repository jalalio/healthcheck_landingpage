<?php

namespace Pluio\Data\NoSQL\Table;

//Microsoft Azure library
use WindowsAzure\Common\ServicesBuilder;
// use MicrosoftAzure\Storage\Common\ServiceException;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use Pluio\Exceptions\RuntimeExceptions\NoSQLRuntimeException;
use Pluio\Logger\Logger;

/**
 * Creates table in Azure Tebles
 */
class CreateTable
{
    /**
     * ServicesBuilder object instance
     *
     * @var object
     */
    private $_db;

    /**
     * Logger object instance
     *
     * @var object
     */
    private $_log;

    /**
     * Creates a table
     *
     * @param  string                $table_name Name for the table
     * @throws ServiceException      On error
     * @throws NoSQLRuntimeException On error
     * @return bool
     */
    public function execute(string $table_name)
    {
        try {
            try {
                $this->_db->createTable($table_name);
                $this->_log->system(1, 'NoSQL Table created successfully', 8026);
                return true;
            } catch (ServiceException $e) {
                // Handle exception based on error codes and messages.
                // Error codes and messages can be found here:
                // http://msdn.microsoft.com/library/azure/dd179438.aspx
                throw new NoSQLRuntimeException($e->getMessage(), $e->getCode());
            }
        } catch (NoSQLRuntimeException $er) {
            $er->issue('error');
            $er->system(3, 'NoSQL Table operation failed', 8025);
            return false;
        }
    }

    /**
     * Gets the connection to the vendor
     *
     * @param string $vendor Database infrastructure
     */
    public function __construct($vendor = 'Azure')
    {
        //Database
        $db = new \Pluio\Data\NoSQL\Table\Db($vendor);
        $this->_db = $db->tableRestProxy();
        //Logger
        $this->_log = new Logger();
        return;
    }
}
