-- --------------------------------------------------------
-- Host:                         jannahabudo.mysql.database.azure.com
-- Server version:               5.7.18-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for healthcheck
CREATE DATABASE IF NOT EXISTS `healthcheck` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `healthcheck`;

-- Dumping structure for table healthcheck.healthcheck
CREATE TABLE IF NOT EXISTS `healthcheck` (
  `ping` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table healthcheck.healthcheck: ~0 rows (approximately)
/*!40000 ALTER TABLE `healthcheck` DISABLE KEYS */;
INSERT IGNORE INTO `healthcheck` (`ping`) VALUES
	('+PONG');
/*!40000 ALTER TABLE `healthcheck` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
