<?php

namespace Pluio;

/**
 * Canned messages
 */
class Messages
{
    public $hostname;
    public $ip_prober;
    public $pong;
    public $alert;

    public function __construct(string $finger = '')
    {
        $this->hostname = appName();
        $this->ip_prober = $_SERVER['REMOTE_ADDR'];
        //Load the composed messages so we can use the properties right off object instantiation
        $this->pong = $this->standard();
        $this->alert = $this->panpan($finger);
        return;
    }

    /**
     * Signal ping events
     */
    public function standard()
    {
        $template = getenv('HEALTHCHECKER_MESSAGE_PING');
        return sprintf($template, $this->ip_prober, $this->hostname);
    }

    /**
     * Signal error level events
     * @param $finger Service reported failed
     */
    public function panpan(string $finger)
    {
        $template = getenv('HEALTHCHECKER_MESSAGE_ALERT');
        return sprintf($template, $finger, $this->hostname);
    }
}
