<?php

// include_once __DIR__ . '/../bootstrap.php';

use PHPUnit\Framework\TestCase;
use Pluio\Monitors\RedisMonitor;

class RedisMonitorTest extends TestCase
{
    private $_redis;

    public function setUp()
    {
        $this->_redis = new RedisMonitor();
    }

    /** @test */
    public function the_server_responds_pong_to_a_ping()
    {
        $test = $this->_redis->ping();
        $this->assertNotFalse($test, 'RedisMonitor returned false');
    }
}
