<?php

use PHPUnit\Framework\TestCase;
use Pluio\Logger;

class LoggerTest extends TestCase
{
    /** @test */
    public function the_logger_path_is_writtable()
    {
        $filepath = new Logger();
        $path = $filepath->getLogPath();
        $this->assertNotFalse(is_writable($path), 'The logger path is not writtable');
    }
}
