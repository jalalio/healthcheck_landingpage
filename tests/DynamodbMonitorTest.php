<?php

use PHPUnit\Framework\TestCase;
use Pluio\Monitors\DynamodbMonitor;

class DynamodbMonitorTest extends TestCase
{
    private $_dynamodb_ping;

    public function seUp()
    {
        $redisMonitor = new DynamodbMonitor();
        $this->_redis_ping = $redisMonitor->ping();
        return;
    }

    public function testPingReturnsBool()
    {
        $this->assertNotFalse($this->_dynamodb_ping, "DynamodbMonitor didn't return boolean");
    }
}
