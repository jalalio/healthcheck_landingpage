<?php

namespace Pluio\Monitors;

use MongoDB\Client as Mongo;
use Pluio\Logger;
use Pluio\Communication\Slack;

class MongodbMonitor
{
    /**
     * Hostname
     */
    private $_host;

    /**
     * Port
     */
    private $_port;

    /**
     * DB Username
     */
    private $_user;

    /**
     * DB Password
     */
    private $_password;

    /**
     * DB Database name
     */
    private $_benchmark_db;

    /**
     * Pluio\Logger instance handler
     */
    private $_logger;

    /**
     * Slack object instance
     */
    private $_slack;

    /**
     * DB credentials
     *
     * @param $host Hostname
     * @param $user Username
     * @param $password Password
     * @param $benchmark_db Name of the database to probe
     */
    public function __construct(string $host = '', int $port = 27017, string $user = '', string $password = '', string $benchmark_db = '')
    {
        if ($host) {
            $this->_host = $host;
            $this->_port = $port;
            $this->_user = $user;
            $this->_password = $password;
            $this->_benchmark_db = $benchmark_db;
        } else {
            $this->_host = \getenv('MONGODB_HOST');
            $this->_port = \getenv('MONGODB_PORT');
            $this->_user = \getenv('MONGODB_USERNAME');
            $this->_password = \getenv('MONGODB_PASSWORD');
            $this->_benchmark_db = \getenv('MONGODB_DATABASE');
        }

        $this->_slack = new Slack();
        $this->_logger = new Logger();

        return $this;
    }

    /**
     * Runs a SELECT query against the DB
     *
     * @param $query A valid SQL query to run on the DB
     */
    public function ping(array $query)
    {
        $checkup = false;
        try {
            $mongo = new Mongo("mongodb://{$this->_user}:{$this->_password}@{$this->_host}:{$this->_port}/{$this->_benchmark_db}");

            $collection = $mongo->healthcheck->heartbeat;
            $cursor = $collection->find();
            foreach ($cursor as $document) {
                if ($document["ping"] == 'PONG') {
                    $checkup = true;
                    break;
                }
            }
        } catch (PDOException $e) {
            $this->_slack->post('MongoDB Monitor Error: ' . $e->getMessage(), 'danger');
            $this->_logger->error('MongoDB Monitor Error: ' . $e->getMessage());
        }
        return $checkup;
    }
}
