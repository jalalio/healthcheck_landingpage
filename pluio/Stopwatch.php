<?php

namespace Pluio;

/**
 * Measures time elapsed between a given start Intervals and End Signals or since the
 * server received the request
 *
 * @see http://php.net/getrusage
 * @see http://php.net/manual/en/function.microtime.php
 */
class Stopwatch
{
    /**
     * Signals starting time
     */
    private $_startTime;

    /**
     * Signals Ending time
     *
     */
    private $_endTime;

    /**
     * Name of process profiled
     */
    private $_processName;

    /**
     * unit for displaying time
     */
    private $_unit = 'sec';

    /**
     * 'usec' => microseconds ; 
     * 'sec' => seconds ; 
     * 'msec' => 'milliseconds'
     */
    private $_units_available = ['usec', 'sec', 'msec'];

    /**
     * Holds $_SERVER["REQUEST_TIME_FLOAT"] value
     */
    private $_requestTimestamp;

    /**
     * Stack of Intervals
     */
    private $_stack = array();

    /**
     * Counts the number of intervals in the stack
     */
    private $_interval_pointer = 1;

    /**
     * @param $processName Name of the process to chrono
     * @param $unit
     */
    public function __construct(string $processName)
    {
        $this->_processName = $processName;
        $this->_requestTimestamp = $_SERVER["REQUEST_TIME_FLOAT"];
    }

    /**
     * Starts the Chrono
     * 
     * @return $this
     */
    public function setStart()
    {
        $this->_startTime = \microtime(true);
        return $this;
    }

    /**
     * Gets the value for a named interval
     * 
     * @param $name Label of the interval
     * @return float || null
     */
    public function getInterval(string $name, string $unit = null)
    {
        foreach($this->_stack as $interval)
        {
            if(is_array($interval))
            {
                foreach($interval as $k => $v)
                {
                    if($v == $name)
                    {
                        $timestamp = $interval['timestamp'];
                    }
                    
                }
            }
            
        }
        if($timestamp)
        {
            $this->setUnit($unit);
            if(!$this->_startTime)     
            {
                $this->_startTime = $this->_requestTimestamp;
            }
            return $this->_unitConverter($timestamp - $this->_startTime);
        }
        return null;
    }

    /**
     * Sets an Interval (lap) in order to segment readings
     * 
     * @param $name Label to assign to the Interval
     */
    public function setInterval(string $name = '')
    {
        //If no start time has been signaled, use the Request time instead
        if(!$this->_startTime)
        {
            $this->_stack['Start'] = $this->_requestTimestamp;
        } else {
            $this->_stack['Start'] = $this->_startTime;
        }
        if(!$name)
        {
            $name = 'Interval ' . (string) $this->_interval_pointer;
        }
        $this->_stack[$this->_interval_pointer] = array(
            'pointer' => $this->_interval_pointer,
            'label' => $name,
            'timestamp' => \microtime(true)
        );
        $this->_interval_pointer ++;        
        return;
    }

    /**
     * Stops the Chrono
     * 
     * @return $this
     */
    public function setEnd()
    {     
        $this->_endTime = $this->_stack['End'] = \microtime(true);
        return $this;
    }
    
    /**
     * Gets the total runtime of the chrono
     * 
     * @param $unit Time unit for the values (Seconds, Milliseconds or Microsecods)
     * @return float
     */
    public function getRuntime(string $unit = null)
    {
        $this->setUnit($unit);
        if(!$this->_endTime)        
        {
            //Set the End as the last reading from the Stack
            $this->_endTime = array_pop($this->_stack);
        } else {
            //End is now
            $this->_endTime = \microtime(true);
        }
        $total = $this->_endTime - $this->_startTime;
        return $this->_unitConverter($total);
    }

    /**
     * Gets the total timed intervals and computed total time
     * 
     * @param $unit Time unit for the values (Seconds, Milliseconds or Microsecods)
     * @return Array
     */
    public function getSplicedRutime(string $unit = null)
    {
        $this->setUnit($unit);
        if(!$this->_endTime && $this->_stack)        
        {
            //Set the End as the last reading from the Stack
            $this->_endTime = array_pop($this->_stack);
        } else {
            //End is now
            $this->_endTime = \microtime(true);
        }
        if(!$this->_interval_pointer)        
        {
            return $this->getRuntime($unit);
        } else {
            $leaderboard = array();
            for($i = 1; $i < $this->_interval_pointer; $i++)
            {
                if($i == 1)
                {
                    if($this->_stack[$i]['label'])
                    {
                        $label = $this->_stack[$i]['label'];
                    } else {
                        $label = "Interval {$i}";
                    }
                    $leaderboard[$label] = $this->_unitConverter( $this->_stack[$i]['timestamp'] - $this->_startTime);
                } elseif($i == $this->_interval_pointer) 
                { 
                    if($this->_stack[$i]['label'])
                    {
                        $label = $this->_stack[$i]['label'];
                    } else {
                        $label = "Interval {$i}";
                    }
                    $leaderboard[$label] = $this->_unitConverter( $this->_endTime - $this->_stack[$i]['timestamp']);
                } else {
                    if($this->_stack[$i]['label'])
                    {
                        $label = $this->_stack[$i]['label'];
                    } else {
                        $label = "Interval {$i}";
                    }
                    $leaderboard[$label] = $this->_unitConverter( $this->_stack[$i]['timestamp'] - $this->_stack[$i - 1]['timestamp']);
                }
            }
        }
        $total = $this->_endTime - $this->_requestTimestamp;
        $leaderboard['total'] = $this->_unitConverter($total);
        return $leaderboard;
    }

    /**
     * Computes the time elapsed from the moment the Server received
     * the Request to the time signaled as End Time or when the method
     * is called
     * 
     * @return float
     */
    public function getGlobalRuntime(string $unit = null)
    {
        $this->setUnit($unit);
        if(!$this->_endTime)        
        {
            //Set the End as the last reading from the Stack
            $this->_endTime = array_pop($this->_stack);
        } else {
            //End is now
            $this->_endTime = \microtime(true);
        }
        $total = $this->_endTime - $this->_requestTimestamp;
        return $this->_unitConverter($total);
    }

    /**
     * Banner with the readings' totals
     * 
     * @return string
     */
    public function __toString()
    {
        return "The Process \"{$this->_processName}\" used {$this->getRuntime()} {$this->getunit()}.\nThe System spent {$this->getGlobalRuntime()} {$this->getunit()} since the request was received.\n";
    }

    /**
     * Sets the Time Unit
     */
    public function setUnit($unit)
    {
        if (in_array($unit, $this->_units_available)) {
            $this->_unit = $unit;
        }
        return $this;
    }

    /**
     * Gets the Time Unit active for the readings
     * 
     * @return string
     */
    public function getUnit()
    {
        $units = [
            'usec' => 'Microseconds',
            'msec' => 'Milliseconds',
            'sec' => 'Seconds'
        ];
        return $units[$this->_unit];
    }

    /**
     * Converts from Seconds into Milliseconds and Microseconds
     * 
     * @return float
     */
    private function _unitConverter($value)
    {
        switch ($this->_unit) {
            case 'usec':
                return $value * 1000000;
                break;
            case 'msec':
                return $value * 1000;
                break;
            case 'sec':
            default:
            break;
        }
        return $value;
    }
}