<?php

namespace Pluio\Communication\Slack\WebHook;

//Taken from
//@link https://raw.githubusercontent.com/SimonBackx/Slack-PHP-Webhook/master/slack.php
/*
	The MIT License (MIT)
	
	Copyright (c) 2015 Simon Backx
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

/**
	*  Main Object. Construct it by passing your webhook url from slack.com (e.g. https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX)
	*  Needed for posting Slack Messages
*/

class SlackAttachmentField 
{
	// Required
	public $title = "";
	public $value = "";
	
	// Optional
	public $short;

	function __construct($title, $value, $short = NULL) {
		$this->title = $title;
		$this->value = $value;
		if (isset($short)){
			$this->short = $short;
		}
	}

	function setShort($bool = true) {
		$this->short = $bool;
		return $this;
	}
	
	function toArray() {
		$data = array(
			'title' => $this->title,
			'value' => $this->value
		);
		if (isset($this->short)){
			$data['short'] = $this->short;
		}
		return $data;
	}
}