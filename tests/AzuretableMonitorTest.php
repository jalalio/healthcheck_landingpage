<?php


use PHPUnit\Framework\TestCase;
use Pluio\Monitors\AzuretableMonitor;

class AzureMonitorTest extends TestCase
{
    private $_azure_table;

    public function setUp()
    {
        $table = new AzuretableMonitor();
        $this->_azure_table = $table->ping();
    }

    /** @test */
    public function the_azure_healtchceck_table_returns_pong()
    {
        $this->assertNotFalse($this->_azure_table, 'Azure Table is not responding PONG to the PING');
    }
}
