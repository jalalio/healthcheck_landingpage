<?php


use PHPUnit\Framework\TestCase;
use Pluio\Monitors\FilesystemMonitor;

class FilesystemTest extends TestCase
{
    /** @test */
    public function check_if_app_directory_is_writtable()
    {
        $filesystem = new FilesystemMonitor(\getenv('HEALTHCHECK_FILESYSTEM_PATH'));
        $this->assertNotFalse($filesystem->isWritable(), 'Filesystem is not writtable');
    }
}
